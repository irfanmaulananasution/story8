from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.test import LiveServerTestCase
from .views import loginUser, logoutUser, home
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time

from django.contrib.auth.models import User

class IndexUnitTest(TestCase):
	def test_index_url_exists(self):
		response = Client().get('/auth/login/')
		self.assertEqual(response.status_code, 200)

	def test_using_index_view(self):
		found = resolve('/auth/login/')
		self.assertEqual(found.func, loginUser)

	def test_using_index_template(self):
		response = Client().get('/auth/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_login(self):
		user = User.objects.create(username='ehem')
		user.set_password('12345')
		user.save()

		c = Client()
		logged_in = c.login(username='ehem', password='12345')
		self.assertTrue(logged_in, True)

	def test_login_template_used(self):
		user = User.objects.create(username='ehem')
		user.set_password('12345')
		user.save()

		c = Client()
		logged_in = c.login(username='ehem', password='12345')
		
		response = c.get('/auth/home/')
		self.assertTemplateUsed(response, 'home.html')

	def test_login_template_used(self):
		user = User.objects.create(username='ehem')
		user.set_password('12345')
		user.save()

		c = Client()
		logged_in = c.login(username='wadaw', password='12345')
		
		response = c.get('/auth/home/')
		self.assertRedirects(response, '/auth/login/', status_code=302, target_status_code=200, fetch_redirect_response=True)
	
	def test_request_method(self):
		user = User.objects.create(username='ehem')
		user.set_password('12345')
		user.save()

		c = Client()

		response = self.client.post(reverse('login'), {'username': 'ehem', 'password' : '12345'})
	
	def test_open_logout(self):
		user = User.objects.create(username='ehem')
		user.set_password('12345')
		user.save()

		c = Client()

		logged_in = c.login(username='ehem', password='12345')

		response = c.get('/auth/logout/')
		self.assertTemplateUsed(response, 'logout.html')

	def test_redirect_logout_to_login(self):
		c = Client()
		response = c.get('/auth/logout/')
		self.assertRedirects(response, '/auth/login/', status_code=302, target_status_code=200, fetch_redirect_response=True)
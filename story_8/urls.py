from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('book/', include('home.urls')),
    path('auth/', include('login.urls')),
    path('', include('landingPage.urls')),
]
